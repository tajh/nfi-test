grep -B10 age4 testPackage/git.html | grep -A1 'age4\|<td class="hidden-phone"><span class="list">' | grep -o '[-a-zA-Z0-9\/]*\.git\|[0-9]* weeks\|[0-9]* months' |  paste -s -d ' \n' - - | awk '{if ($3 ~ /months/) multiplier=31; else if ($3 ~ /weeks/) multiplier=7; printf "Repo %s has not been updated in approximately %s %s which is roughly %d days\n", $1, $2, $3, $2*multiplier }'

