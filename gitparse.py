#!/usr/bin/env python
from bs4 import BeautifulSoup
import sys

# we use bs4 to parse the html
soup = BeautifulSoup(sys.stdin.read(), 'html.parser')

# we search for all the entries that have a convenient class of age4
# (that class is generated for all items that are 4 weeks or older)
for agefour in soup.find_all( class_="age4"):
    # the great grand parent of that item will contain the git repo name
    greatgramps = agefour.parent.parent.parent
    gitpath = greatgramps.find('a').get('href')
    reponame = gitpath[gitpath.rfind("=")+1:len(gitpath)]
    # we break up the age string
    age = agefour.get_text().split()
    if age[1]=="months":
        # we estimate the age in days when given a number in months
        ageindays = 30.5 * int(age[0])
    elif age[1]=="weeks":
        # we estimate the age in days when given a number in weeks
        ageindays = 7 * int(age[0])
    if ageindays > 29:
        # we output our estimation of the repo's age
        print( "The repo %s was last updated %d days ago." % (reponame, ageindays) )
