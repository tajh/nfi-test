## Sapient DevOps Set1 Test Response

Test items:

    Set 1: Duration 2 hours
    1.	Using git.html in the test package, create and share a script to parse this html and generate a list containing repositories which haven’t been updated for last 30 days,  list should contain repository name and duration since it has not change in days.
    2.	Create a script using which one can install any Jenkins plugin (eg. git) and along with all its dependencies in a single shot.

### Test response item 1:

I had one minor problem in that the source file does not actually list the age of the git repositories in days, so there is high level of inaccuracy in the results. The source data has low accuracy for items over 27 days old.  The data requested is for items 30 days or older, which we cannot accurately determine at least two items in the provided test data set.

I have created two solutions to this query:

* a shell one liner (gitparse.sh and its output in gitparse.sh.out.txt - generated with `./gitparse.sh > gitparse.sh.out.txt`):

      grep -B10 age4 testPackage/git.html | grep -A1 'age4\|<td class="hidden-phone"><span class="list">' | grep -o '[-a-zA-Z0-9\/]*\.git\|[0-9]* weeks\|[0-9]* months' |  paste -s -d ' \n' - - | awk '{if ($3 ~ /months/) multiplier=31; else if ($3 ~ /weeks/) multiplier=7; printf "Repo %s has not been updated in approximately %s %s which is roughly %d days\n", $1, $2, $3, $2*multiplier }'

* a python script (gitparse.py and its output in gitparse.py.out.txt - generated with `./gitparse.py < testPackage/git.html > gitparse.py.out.txt`):

      #!/usr/bin/env python
      from bs4 import BeautifulSoup
      import sys

      # we use bs4 to parse the html
      soup = BeautifulSoup(sys.stdin.read(), 'html.parser')

      # we search for all the entries that have a convenient class of age4
      # (that class is generated for all items that are 4 weeks or older)
      for agefour in soup.find_all( class_="age4"):
          # the great grand parent of that item will contain the git repo name
          greatgramps = agefour.parent.parent.parent
          gitpath = greatgramps.find('a').get('href')
          reponame = gitpath[gitpath.rfind("=")+1:len(gitpath)]
          # we break up the age string
          age = agefour.get_text().split()
          if age[1]=="months":
              # we estimate the age in days when given a number in months
              ageindays = 30.5 * int(age[0])
          elif age[1]=="weeks":
              # we estimate the age in days when given a number in weeks
              ageindays = 7 * int(age[0])
          if ageindays > 29:
              # we output our estimation of the repo's age
              print( "The repo %s was last updated %d days ago." % (reponame, ageindays) )

### Test response item 2:

I always use docker for jenkins, and therefore use the official install script (https://github.com/jenkinsci/docker/blob/master/install-plugins.sh), which already manages plugin dependencies automatically at container runtime.  I have included a bash script installjenkinsplugin.sh written by micw that downloads plugins and their dependencies automatically.
